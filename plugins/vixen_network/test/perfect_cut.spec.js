const plugin = require("../main");
const { expect } = require("chai");
const { createPluginRunner } = require("../../../context");

const runPlugin = createPluginRunner("vixen_network", plugin);

describe.skip("VIXEN network", () => {
  describe("TUSHY - Perfect Cut", () => {
    it(`Basic: Should get thumbnail`, async () => {
      const result = await runPlugin({
        event: "sceneCreated",
        sceneName: "===???",
        scene: {
          _id: "xxx",
          name: "?????????????",
          path: "Perfect Cut - TUSHY",
        },
        args: {
          useChapters: false,
        },
      });
      expect(result.$thumbnail).to.be.a("string").that.contains("mainLandscape");
    });
  });
});
