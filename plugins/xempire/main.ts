import axios from "axios";
import { applyMetadata, Plugin, Context } from "../../types/plugin";
import { SceneOutput } from "../../types/scene";

import info from "./info.json";

type MyContext = Context & { scenePath?: string; sceneName?: string };

const refererUrl = "https://www.xempire.com/";
const userAgent =
  "Algolia for vanilla JavaScript (lite) 3.27.0;instantsearch.js 2.7.4;JS Helper 2.26.0";

async function getCredentials($axios: typeof axios) {
  const res = await $axios.get(refererUrl, {
    responseType: "text",
  });
  const body = res.data;
  const apiLine = body.split("\n").find((bodyLine: string) => bodyLine.match("apiKey"));
  if (!apiLine) {
    throw new Error(`Could not extract XEmpire API Key from ${refererUrl}`);
  }
  const apiSerial = apiLine.slice(apiLine.indexOf("{"), apiLine.indexOf("};") + 1);
  const apiData = JSON.parse(apiSerial);

  const { applicationID: appId, apiKey } = apiData.api.algolia;

  const apiUrl = `https://${appId.toLowerCase()}-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=${userAgent}&x-algolia-application-id=${appId}&x-algolia-api-key=${apiKey}`;

  return { appId, apiKey, apiUrl };
}

function getImageUrl(path: string): string {
  return `https://images01-evilangel.gammacdn.com/movies${path}`;
}

const handler: Plugin<MyContext, SceneOutput> = async (ctx) => {
  const { $createImage, $moment, $logger, $axios, sceneName, scenePath, event } = ctx;

  const q = sceneName || scenePath;

  if (!q) {
    $logger.warn(`Invalid event: ${event}`);
    return {};
  }
  
  if (!["hardx","darkx","lesbianx","xempire","allblackx","eroticax"].some(x => q.toLowerCase().includes(x))) {
    $logger.verbose(`No XEmpire Studio found in: ${q}`);
    return {};
  }

  const { apiUrl } = await getCredentials($axios);

  const algoliaRes = await $axios.post<{
    results?: {
      hits?: {
        title: string;
        // eslint-disable-next-line camelcase
        release_date: string; // YYYY-MM-DD
        actors: {
          name: string;
          gender: "male" | "female";
        }[];
        categories: {
          name: string;
        }[];
        // eslint-disable-next-line camelcase
        studio_name: string;
        description: string;
        pictures?: {
          nsfw: {
            top: {
              "1920x1080": string;
            };
          };
        };
      }[];
    }[];
  }>(
    apiUrl,
    {
      requests: [
        {
          indexName: "all_scenes",
          params: `query=${q}`,
        },
      ],
    },
    {
      headers: {
        Referer: refererUrl,
      },
    }
  );

  const first = algoliaRes.data.results?.[0]?.hits?.[0];

  if (!first) {
    $logger.warn("No search result");
    return {};
  }

  let thumbnail: string | undefined;
  if (first.pictures?.nsfw?.top?.["1920x1080"]) {
    thumbnail = await $createImage(
      getImageUrl(first.pictures.nsfw.top["1920x1080"]),
      first.title,
      true
    );
  }

  return {
    name: first.title.trim(),
    releaseDate: $moment(first.release_date, "YYYY-MM-DD").toDate().valueOf(),
    description: first.description.trim(),
    actors: first.actors
      .filter((x) => x.gender === "female")
      .map(({ name }) => name.trim())
      .sort(),
    labels: first.categories.map(({ name }) => name.trim()).sort(),
    studio: first.studio_name.trim(),
    thumbnail,
  };
};

handler.requiredVersion = ">=0.27.0";

applyMetadata(handler, info);

module.exports = handler;

export default handler;
