const plugin = require("../main");
const { expect } = require("chai");
const { createPluginRunner } = require("../../../context");

const runPlugin = createPluginRunner("xempire", plugin);

describe("XEmpire", () => {
  describe("LesbianX", () => {
    it("Should get scene by name", async () => {
      const result = await runPlugin({
        sceneName: "hot lesbians lily [lesbianX]",
      });
      expect(result.name).to.equal("Hot Lesbians Lily & Lilly");
      expect(result.labels).to.contain("Blonde");
      expect(result.labels).to.contain("Small Tits");
      expect(result.description).to.equal(
        "Lily Larimar and Lilly Bell are smoking hot in their matching lingerie sets. The blonde beauties can't keep their hands, lips, or bodies off of each other in this sexy scene."
      );
      expect(result.studio).to.equal("LesbianX");
      expect(result.thumbnail).to.be.a("string");
      expect(result.releaseDate).to.equal(1634076000000);
      expect(result.actors).to.deep.equal(["Lilly Bell", "Lily Larimar"]);
    });
  });

  describe("DarkX", () => {
    it("Should get scene by name", async () => {
      const result = await runPlugin({
        sceneName: "Lavishing Lily [darkx]",
      });
      expect(result.name).to.equal("Lavishing Lily");
      expect(result.labels).to.contain("Blonde");
      expect(result.labels).to.contain("Small Tits");
      expect(result.description).to.equal(
        "Isiah Maxwell can't wait to get his mouth and dick on the luscious Lily Larimar. He eats her pussy like a pro before giving the pretty little blonde a proper pounding."
      );
      expect(result.studio).to.equal("DarkX");
      expect(result.thumbnail).to.be.a("string");
      expect(result.releaseDate).to.equal(1649887200000);
      expect(result.actors).to.deep.equal(["Lily Larimar"]);
    });
  });

  describe("HardX", () => {
    it("Should get scene by name", async () => {
      const result = await runPlugin({
        sceneName: "Lily Tells All [HARDX]",
      });
      expect(result.name).to.equal("Lily Tells All");
      expect(result.labels).to.contain("Blonde");
      expect(result.labels).to.contain("Small Tits");
      expect(result.description).to.equal(
        "Lily Larimar tells us about the time she lost her virginity and other naughty deeds she's done before getting into this steamy creampie action with Mick Blue."
      );
      expect(result.studio).to.equal("HardX");
      expect(result.thumbnail).to.be.a("string");
      expect(result.releaseDate).to.equal(1635544800000);
      expect(result.actors).to.deep.equal(["Lily Larimar"]);
    });

    it("Should get scene by name 2", async () => {
      const result = await runPlugin({
        sceneName: "Lily Larimar - Lily Tells All [HARDX]",
      });
      expect(result.name).to.equal("Lily Tells All");
    });

    it("Should get scene by default name", async () => {
      const result = await runPlugin({
        sceneName: "/disks/folder/Lily Larimar - Lily Tells All - HardX.mp4",
      });
      expect(result.name).to.equal("Lily Tells All");
    });

    it("Should get scene by path", async () => {
      const result = await runPlugin({
        scenePath: "/disks/folder/Lily Larimar - Lily Tells All - HardX.mp4",
      });
      expect(result.name).to.equal("Lily Tells All");
    });
  });
});
