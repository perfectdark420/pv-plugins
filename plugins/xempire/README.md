## xempire 0.0.2

by boi123212321

Scrape XEmpire (HardX, DarkX, EroticaX, LesbianX, AllBlackX)

### Download links
Each download link is for the latest version of the plugin, for the indicated porn-vault server version.  
Make sure you are reading the documentation of the plugin, for the correct porn-vault server version.  
| Server version                                                                                               | Plugin documentation                                                                           |
| ------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------- |
| [Download link for: stable](https://gitlab.com/porn-vault/plugins/-/raw/master/dist/xempire.js?inline=false) | [documentation](https://gitlab.com/porn-vault/plugins/-/blob/master/plugins/xempire/README.md) |


### Example installation with default arguments

`config.json`

```json
---
{
  "plugins": {
    "register": {
      "xempire": {
        "path": "./plugins/xempire.js",
        "args": {}
      }
    },
    "events": {
      "sceneCreated": [
        "xempire"
      ],
      "sceneCustom": [
        "xempire"
      ]
    }
  }
}
---
```

`config.yaml`

```yaml
---
plugins:
  register:
    xempire:
      path: ./plugins/xempire.js
      args: {}
  events:
    sceneCreated:
      - xempire
    sceneCustom:
      - xempire

---

```
