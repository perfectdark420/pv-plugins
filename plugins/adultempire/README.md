## adultempire 0.6.0

by boi123212321

Scrape data from adultempire

### Download links
Each download link is for the latest version of the plugin, for the indicated porn-vault server version.  
Make sure you are reading the documentation of the plugin, for the correct porn-vault server version.  
| Server version                                                                                                   | Plugin documentation                                                                               |
| ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| [Download link for: stable](https://gitlab.com/porn-vault/plugins/-/raw/master/dist/adultempire.js?inline=false) | [documentation](https://gitlab.com/porn-vault/plugins/-/blob/master/plugins/adultempire/README.md) |


### Documentation

## Plugin Details

This plugin retrieves data from adultempire.

NOTE: Unfortunately, sometimes movie front covers are uploaded as back covers, which can't be easily detected, so sometimes there may be false positives when downloading back covers.



### Arguments

| Name | Type    | Required | Description                    |
| ---- | ------- | -------- | ------------------------------ |
| dry  | Boolean | false    | Whether to commit data changes |

### Example installation with default arguments

`config.json`

```json
---
{
  "plugins": {
    "register": {
      "adultempire": {
        "path": "./plugins/adultempire.js",
        "args": {
          "dry": false
        }
      }
    },
    "events": {
      "movieCreated": [
        "adultempire"
      ],
      "actorCreated": [
        "adultempire"
      ]
    }
  }
}
---
```

`config.yaml`

```yaml
---
plugins:
  register:
    adultempire:
      path: ./plugins/adultempire.js
      args:
        dry: false
  events:
    movieCreated:
      - adultempire
    actorCreated:
      - adultempire

---

```
