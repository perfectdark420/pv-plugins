const plugin = require("../main");
const { expect } = require("chai");
const { createPluginRunner } = require("../../../context");

const runPlugin = createPluginRunner("legalporno_shoot_id", plugin);

describe("EKS vs KS", () => {
  it("Should extract EKS, not KS", async () => {
    const sceneName =
      "First Gangbang Nansy Small vs 4 big cocks ATM DP Deepthroat Rough & fast sex Cum in mouth Swallow EKS089";
    const result = await runPlugin({
      sceneName,
      args: {
        useSceneId: true,
        deep: false,
      },
    });
    expect(result.name).to.equal("EKS089");
  });
});
