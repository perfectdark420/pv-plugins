'use strict';

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn) {
  var module = { exports: {} };
	return fn(module, module.exports), module.exports;
}

var plugin = createCommonjsModule(function (module, exports) {
Object.defineProperty(exports, "__esModule", { value: true });
exports.applyMetadata = void 0;
function applyMetadata(handler, info) {
    handler.info = info;
}
exports.applyMetadata = applyMetadata;
});

var name = "xempire";
var version = "0.0.2";
var authors = [
	"boi123212321"
];
var description = "Scrape XEmpire (HardX, DarkX, EroticaX, LesbianX, AllBlackX)";
var events = [
	"sceneCreated",
	"sceneCustom"
];
var require$$0 = {
	name: name,
	version: version,
	authors: authors,
	description: description,
	events: events,
	"arguments": [
]
};

var __awaiter = (commonjsGlobal && commonjsGlobal.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (commonjsGlobal && commonjsGlobal.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};


const info_json_1 = __importDefault(require$$0);
const refererUrl = "https://www.xempire.com/";
const userAgent = "Algolia for vanilla JavaScript (lite) 3.27.0;instantsearch.js 2.7.4;JS Helper 2.26.0";
function getCredentials($axios) {
    return __awaiter(this, void 0, void 0, function* () {
        const res = yield $axios.get(refererUrl, {
            responseType: "text",
        });
        const body = res.data;
        const apiLine = body.split("\n").find((bodyLine) => bodyLine.match("apiKey"));
        if (!apiLine) {
            throw new Error(`Could not extract XEmpire API Key from ${refererUrl}`);
        }
        const apiSerial = apiLine.slice(apiLine.indexOf("{"), apiLine.indexOf("};") + 1);
        const apiData = JSON.parse(apiSerial);
        const { applicationID: appId, apiKey } = apiData.api.algolia;
        const apiUrl = `https://${appId.toLowerCase()}-dsn.algolia.net/1/indexes/*/queries?x-algolia-agent=${userAgent}&x-algolia-application-id=${appId}&x-algolia-api-key=${apiKey}`;
        return { appId, apiKey, apiUrl };
    });
}
function getImageUrl(path) {
    return `https://images01-evilangel.gammacdn.com/movies${path}`;
}
const handler = (ctx) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d, _e, _f;
    const { $createImage, $moment, $logger, $axios, sceneName, scenePath, event } = ctx;
    const q = sceneName || scenePath;
    if (!q) {
        $logger.warn(`Invalid event: ${event}`);
        return {};
    }
    if (!["hardx", "darkx", "lesbianx", "xempire", "allblackx", "eroticax"].some(x => q.toLowerCase().includes(x))) {
        $logger.verbose(`No XEmpire Studio found in: ${q}`);
        return {};
    }
    const { apiUrl } = yield getCredentials($axios);
    const algoliaRes = yield $axios.post(apiUrl, {
        requests: [
            {
                indexName: "all_scenes",
                params: `query=${q}`,
            },
        ],
    }, {
        headers: {
            Referer: refererUrl,
        },
    });
    const first = (_c = (_b = (_a = algoliaRes.data.results) === null || _a === void 0 ? void 0 : _a[0]) === null || _b === void 0 ? void 0 : _b.hits) === null || _c === void 0 ? void 0 : _c[0];
    if (!first) {
        $logger.warn("No search result");
        return {};
    }
    let thumbnail;
    if ((_f = (_e = (_d = first.pictures) === null || _d === void 0 ? void 0 : _d.nsfw) === null || _e === void 0 ? void 0 : _e.top) === null || _f === void 0 ? void 0 : _f["1920x1080"]) {
        thumbnail = yield $createImage(getImageUrl(first.pictures.nsfw.top["1920x1080"]), first.title, true);
    }
    return {
        name: first.title.trim(),
        releaseDate: $moment(first.release_date, "YYYY-MM-DD").toDate().valueOf(),
        description: first.description.trim(),
        actors: first.actors
            .filter((x) => x.gender === "female")
            .map(({ name }) => name.trim())
            .sort(),
        labels: first.categories.map(({ name }) => name.trim()).sort(),
        studio: first.studio_name.trim(),
        thumbnail,
    };
});
handler.requiredVersion = ">=0.27.0";
plugin.applyMetadata(handler, info_json_1.default);
var main = handler;
var _default = handler;
main.default = _default;

module.exports = _default;
